'use strict';

// original source from https://github.com/maxogden/csv-write-stream

const stream = require('stream');
const util = require('util');
const gen = require('generate-object-property');

// polyfill for RegExp.escape
if (!RegExp.escape) {
    RegExp.escape = function (s) {
        return String(s).replace(/[\\^$*+?.()|[\]{}]/g, '\\$&');
    };
}

function defaultOption (value, defaultValue, disabledValue) {
    if (typeof value === 'undefined' || value === true) {
        value = defaultValue;
    } else if (value === false || value === null) {
        value = disabledValue;
    }
    return value;
}

const through = require('through2');

module.exports = function (opts, cb) {
    if (typeof opts === 'function') {
        cb = opts;
        opts = {}
    }
    opts = opts || {};
    if (typeof cb === 'function') opts.cb = cb;

    opts.sendHeaders = opts.headers !== false;
    opts.headers = opts.headers === true ? null : opts.headers;
    opts.delimiter = defaultOption(opts.delimiter, ',', '');
    opts.newline = defaultOption(opts.newline, '\n', '');
    opts.empty = defaultOption(opts.empty, '', null);
    opts.quote = defaultOption(opts.quote, '"', '');
    opts.escape = defaultOption(opts.escape, '"', '');

    opts._esc = function (quote, escape) {
        return function (cell) {
            if (!quote) {
                return cell;
            }
            let regex = new RegExp(RegExp.escape(quote), 'g');
            return quote + cell.replace(regex, escape + quote) + quote;
        };
    }(opts.quote, opts.escape);

    const state = {
        headers: opts.headers,
        _objRow: null,
        _arrRow: null,
        _first: true
    };

    return createWriter(opts, state).on('error', function (err) {
        if (opts.cb) cb(err)
    })
};

function createWriter(opts, state) {

    function compile(headers) {
        const newline = opts.newline;
        const sep = opts.delimiter;
        const quote = opts.quote;
        const empty = JSON.stringify(opts.empty);
        let regex = '\\s'
            +'|'+ JSON.stringify(RegExp.escape(sep)).slice(1, -1)
            +'|'+ JSON.stringify(RegExp.escape(quote)).slice(1, -1);
        let str = 'return function toRow(obj) {\n';

        if (!headers.length) str += '""';

        headers = headers.map(function (prop, i) {
            str += '  const a' + i + ' = ' + prop + ' === null || '
                + prop + ' === "" ? ' + empty + ' : ' + prop + ';\n';
            return 'a' + i;
        });

        for (let i = 0; i < headers.length; i += 500) { // do not overflow the callstack on lots of cols
            const part = headers.length < 500 ? headers : headers.slice(i, i + 500);
            str += i ? 'result += "' + sep + '" + ' : '  const result = ';
            part.forEach(function (prop, j) {
                str += (j ? '\n    +"' + sep + '"+' : '')
                    + '(/' + regex + '/.test(' + prop + ') ? esc(' + prop + '+"") : ' + prop + ')'
            });
            str += ';\n';
        }

        str += '  return result +' + JSON.stringify(newline) + ';\n};';

        //console.log('Generated formatter: %s', str);

        return new Function('esc', str)(opts._esc);
    }

    return through.obj(opts, function write(row, enc, cb) {
        let isArray = Array.isArray(row);

        if (!isArray && !state.headers) state.headers = Object.keys(row);

        if (state._first && state.headers) {
            state._first = false;

            const objProps = [];
            const arrProps = [];

            for (let i = 0; i < state.headers.length; i++) {
                arrProps.push('obj[' + i + ']');
                objProps.push(gen('obj', state.headers[i]));
            }

            state._objRow = compile(objProps);
            state._arrRow = compile(arrProps);

            if (opts.sendHeaders) this.push(state._arrRow(state.headers));
        }

        if (isArray) {
            if (!state.headers) return cb(new Error('no headers specified'));
            this.push(state._arrRow(row));
        } else {
            this.push(state._objRow(row));
        }

        cb();
    }, function flush(fn) {
        try {
            if (opts.cb) opts.cb();
            fn()
        } catch (err) {
            fn(err)
        }
    })
}
