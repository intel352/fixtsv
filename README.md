#FixTSV
Package to repair TSV files that normally may not be correctly parsed.

##Installation

```
git clone git@bitbucket.org:intel352/fixtsv.git
cd fixtsv
npm install
```

##Usage
```
npm run-script fixtsv convert path/to/badcatalog.tsv path/to/fixedcatalog.tsv
```