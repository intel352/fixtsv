'use strict';

//const path = require('path');

const args = require('args');
const fs = require('fs');

let processing = false;

const flags = args
    .example('fixtsv convert badIn.tsv goodOut.tsv', 'Two arguments are required, the bad file and the good file to output to.')
    .command('convert', 'Convert invalid TSV to valid.', convert)
    .parse(process.argv, {
        name: 'fixtsv'
    });

// executes if no command is run
if (!processing) {
    args.showHelp();
}

function convert(name, sub, options) {
    if (sub.length===2) {
        const fromFile = sub[0];
        const toFile = sub[1];

        processing = true;

        const reader = readTSV(fromFile);
        const writer = writeTSV(toFile);

        let writtenRows = 0;
        let readRows = 0;
        let readerClosed = false;

        writer
            .on('data', function(){
                writtenRows++;
                if (readerClosed && writtenRows >= readRows) {
                    writer.end();
                }
            })
            .on('finish', function(){ // 6
                console.log('Read: %d, Written: %d', readRows, writtenRows);
            });

        reader
            .on('end', function(){
                readerClosed = true;
            })
            .on('data', function (line) {
                readRows++;
            })
            .pipe(writer, {
                end: false
            });
    }
}

function writeTSV(tsvFile) {
    const csvWriter = require('./write-csv');

    const writer = csvWriter({
        delimiter: '\t',
        headers: true,
        quote: '"', // what's considered a quote
        escape: '"',
        empty: '' // empty fields are replaced by this,
    });

    const fileStream = fs.createWriteStream(tsvFile);

    writer.pipe(fileStream);

    return writer;
}

function readTSV(tsvFile) {
    const csv = require('./read-csv');

    const parser = csv({
        delimiter: '\t', // comma, semicolon, whatever
        newline: '\n', // newline character (use \r\n for CRLF files)
        quote: '"', // what's considered a quote
        empty: '', // empty fields are replaced by this,

        // if true, emit arrays instead of stringified arrays or buffers
        objectMode: true,

        // if set to true, uses first row as keys -> [ { column1: value1, column2: value2 }, ...]
        columns: true
    });

    // emits each line as a buffer or as a string representing an array of fields
    const fileStream = fs.createReadStream(tsvFile);

    // now pipe some data into it
    return fileStream.pipe(parser);
}